# TestSSL.sh in GitLab CI

## Instructions

1. Set TARGET variable to the endpoint where you'd like to check TLS/SSL certificate details.
2. `include:remote` the testssl `.gitlab-ci.yml`

  ```yaml
  include:
    - remote: 'https://gitlab.com/greg/testssl/-/raw/master/testssl.gitlab-ci.yml'
  ```
